import pandas as pd
import numpy as np
from datetime import datetime
import xlwings as xw
import os

euclid_file = r'C:\Euclid\Client\Report_Report15_AllPapers.xls'
product_file = r'M:\alk_full_rec\product_map.xlsx'
recon_file = r'M:\alk_full_rec\alkarecon.xlsm'


def fix_numeric_columns_in_csv(df, columns):
    for column in columns:
        if column not in df.columns:
            columns.remove(column)
    df[columns] = df[columns].fillna(0).replace([',|\)'], '', regex=True)
    df[columns] = df[columns].fillna(0).replace(['\(', ], '-', regex=True)
    for columns in columns:
        df[columns] = pd.to_numeric(df[columns], errors='coerce')
    return df


def fix_numeric_columns_in_excel(df, columns):
    for column in columns:
        if column not in df.columns:
            columns.remove(column)
    df[columns] = df[columns].fillna(0).replace([',|\)'], '', regex=True)
    df[columns] = df[columns].fillna(0).replace(['\(', ], '-', regex=True)
    for columns in columns:
        df[columns] = pd.to_numeric(df[columns], errors='coerce')
    return df


def read_csv(filename, rename=None, numeric_columns_to_fix=None):
    df = pd.read_csv(filename, encoding='cp1252')
    if rename:
        df.rename(columns=rename, inplace=True)
    if numeric_columns_to_fix:
        df = fix_numeric_columns_in_csv(df, numeric_columns_to_fix)
    return df


def read_excel(filename, sheetname=None, rename=None, numeric_columns_to_fix=None, header=0):
    df = pd.read_excel(filename, sheet_name=sheetname, header=header)
    if numeric_columns_to_fix:
        df = fix_numeric_columns_in_excel(df, numeric_columns_to_fix)
    if rename:
        df.rename(columns=rename, inplace=True)
    return df


def read_csv_without_pandas(filename):
    import csv
    with open(filename) as file:
        reader = csv.reader(file)
        df = pd.DataFrame([row for row in reader])
    return df


def read_product_map(filename, sheet_name=0):
    df = read_excel(filename, sheetname=sheet_name)
    return df


def prepare_euclid(filename, sheet_name=0):
    df = read_excel(filename, sheetname=sheet_name, rename=None, numeric_columns_to_fix=None, header=0)
    end_date = df.iloc[0, 10]
    end_date = pd.to_datetime(end_date, format='%d.%b.%Y')
    df.columns = df.loc[2]
    df = df.iloc[3:]
    df['EndDate'] = end_date
    columns = ['EndDate', 'Type', 'B/S', 'Quantity', 'Unit', 'Contract', 'Maturity', 'Price', 'MtM', 'Price A',
               'Price B',
               'Flavor', 'Strike', 'Cl. Broker', 'Cl. Account', 'From', 'To', 'Expiry date', 'Commodity A',
               'Commodity B', 'TPA']
    df = df[columns]
    df['Quantity'] = pd.to_numeric(df['Quantity'], errors='coerce')
    df['Unit'] = df['Unit'].str.upper()
    df['Price'] = pd.to_numeric(df['Price'], errors='coerce')
    df['MtM'] = pd.to_numeric(df['MtM'], errors='coerce')
    df['Strike'] = pd.to_numeric(df['Strike'], errors='coerce')
    df['Expiry date'] = pd.to_datetime(df['Expiry date'], errors='coerce')
    df['To'] = pd.to_datetime(df['To'], errors='coerce')
    df['From'] = pd.to_datetime(df['From'], errors='coerce')
    df['Maturity'] = pd.to_datetime(df['Maturity'], format='%Y-%m-%d')
    return df, end_date


def euclid_manipulations(df):
    df.loc[df['Maturity'].isnull(), 'Maturity'] = df['To']
    df['Type'] = df['Type'].str.upper()
    df = df.loc[df['Expiry date'] > df['EndDate']].reset_index()
    df = df.loc[~(df['TPA'].isin(['TPA']) & (df['Cl. Account'].isin(['LEN33122', '8781', '8803', '8989', '9016'])))]
    df.drop(['index', 'TPA'], axis=1, inplace=True)
    df = df.rename(columns={'Cl. Account': 'ACCOUNT', 'Type': 'TYPE', 'Unit': 'UNIT'})
    df['Maturity'] = df['Maturity'].dt.strftime('%Y-%m')
    df.loc[df['TYPE'].isin(['SWAP']), 'Contract'] = df['Commodity B']
    return df


### Euclid starts ###

euclid = prepare_euclid(euclid_file)
date = euclid[1].strftime('%Y%m%d')
euclid = euclid[0]
euc = euclid_manipulations(euclid)

pm = read_product_map(product_file, sheet_name=0)
pm = pm.loc[~pm['CODE'].isin(['XXX'])]
pm['TYPE'] = pm['TYPE'].str.upper()
pm = pm.loc[pm['TYPE'] == 'CFD']
pm = pm[['TYPE', 'Reco Product', 'Leg1', 'Leg2']]

euc = euc.merge(pm.drop_duplicates().reset_index(drop=True), left_on=['TYPE', 'Commodity A', 'Commodity B'],
                right_on=['TYPE', 'Leg1', 'Leg2'], how='left')
euc.loc[euc['TYPE'].isin(['CFD']), 'Contract'] = euc['Reco Product']

euclid_report = euc[
    ['EndDate', 'TYPE', 'B/S', 'Quantity', 'UNIT', 'Contract', 'Maturity', 'Commodity A', 'Commodity B', 'Price',
     'From', 'To', 'Expiry date', 'Strike', 'Flavor']]
euc.drop(['Reco Product', 'Leg1', 'Leg2', 'From', 'To', 'Commodity A', 'Commodity B', 'Expiry date', 'Strike', 'Flavor',
          'B/S', 'Cl. Broker', 'Price A', 'Price B', 'MtM'], axis=1, inplace=True)
euc = euc.rename(columns={'Contract': 'Reco Product'})
euc['Price'] = euc['Price'].apply(lambda x: round(x, 2))
euc['CLEARER'] = 'EUCLID'


def get_report_enddates():
    return f'Euclid report EndDate: {date}'


reports_enddate = get_report_enddates()


def get_fcstone_file():
    for root, dirs, files in os.walk(r'K:\\'):
        for file in files:
            if file.endswith(".csv"):
                if 'detailedopenpositionstradenumber' in os.path.splitext(os.path.basename(file))[0].lower():
                    if file.split('_')[0] == date:
                        return file


fcstone_file = get_fcstone_file()


def prepare_fcstone(filename):
    rename = {'COB': 'EndDate', 'MAT_DATE': 'Maturity', 'TRADE_PRICE': 'Price', 'PRODUCT_DESCRIPTION': 'Instrument',
              'PHYSICAL_CODE': 'CODE'}
    df = read_csv(filename, rename=rename)
    cols = ['EndDate', 'Instrument', 'CODE', 'Maturity', 'CONTRACT_SIZE', 'QTY', 'Price', 'QTY_UNIT', 'FAMILY_ACCOUNT',
            'CLEARER']
    df = df[cols]
    df['CONTRACT_SIZE'] = pd.to_numeric(df['CONTRACT_SIZE'], errors='coerce')
    df['QTY'] = df['QTY'].apply(pd.to_numeric, errors='coerce')
    df['Price'] = df['Price'].apply(pd.to_numeric, errors='coerce')
    df['Quantity'] = df['QTY'] * df['CONTRACT_SIZE']
    df['EndDate'] = pd.to_datetime(df['EndDate'], errors='coerce')
    df['Maturity'] = pd.to_datetime(df['Maturity'], errors='coerce')
    return df


# fcstone_file = os.path.join('K:\\', fcstone_file)
#
# try:
#     fcstone_file = os.path.join('K:\\', fcstone_file)
# except TypeError:
#     fcstone_file = None
#
# if fcstone_file is None:
#     fcstone_file_not_found_message = f'Check if FcStone file exists on date {date}'
# else:
#     fcstone_file_not_found_message = ''
#
# try:
#     fcstone = prepare_fcstone(fcstone_file)
# except ValueError:
#     fcstone = pd.DataFrame(columns=['EndDate', 'Instrument', 'CODE', 'Maturity', 'CONTRACT_SIZE', 'QTY', 'Price', 'QTY_UNIT', 'FAMILY_ACCOUNT', 'CLEARER', 'Quantity'])

fcstone_file = r'M:\alk_full_rec\20210921_INTLFCSTONE_LEN33120_DetailedOpenPositionsTradeNumber.csv'


def fcstone_manipulations(fcs):
    fcs.loc[fcs['QTY_UNIT'] == 'TONNE', 'QTY_UNIT'] = 'MT'
    fcs.loc[fcs['QTY_UNIT'] == 'BARREL', 'QTY_UNIT'] = 'BBL'
    fcs.loc[fcs['QTY_UNIT'] == 'BARRELS', 'QTY_UNIT'] = 'BBL'
    fcs.loc[fcs['QTY_UNIT'] == 'MET TON', 'QTY_UNIT'] = 'MT'
    fcs.loc[fcs['QTY_UNIT'] == 'METRIC TONNE', 'QTY_UNIT'] = 'MT'
    fcs.loc[fcs['QTY_UNIT'] == 'OZ', 'QTY_UNIT'] = 'OZ'
    fcs = fcs.rename(columns={'QTY_UNIT': 'UNIT'})
    fcs['TYPE'] = fcs['TYPE'].str.upper()
    fcs = fcs.rename(columns={'FAMILY_ACCOUNT': 'ACCOUNT'})
    fcs['Maturity'] = fcs['Maturity'].dt.strftime('%Y-%m')
    fcs.drop(['Instrument', 'CONTRACT_SIZE', 'QTY', 'CODE'], axis=1, inplace=True)
    fcs['Price'] = fcs['Price'].apply(lambda x: round(x, 2))
    return fcs


### Fc Stone starts ###
fcstone = prepare_fcstone(fcstone_file)
fcs = fcstone.copy()
fcs_report = fcs.copy()

pm_ = read_product_map(product_file, sheet_name=0)
pm_['TYPE'] = pm_['TYPE'].str.upper()
pm_ = pm_.loc[~pm_['CODE'].isin(['XXX'])]

fcs = fcs.merge(pm_[['CODE', 'TYPE', 'Reco Product']], on='CODE', how='left')

try:
    fcs_final = fcstone_manipulations(fcs)
except (KeyError, AttributeError):
    fcs_final = pd.DataFrame(
        columns=['EndDate', 'Maturity', 'Price', 'ACCOUNT', 'CLEARER', 'Quantity', 'TYPE', 'Reco Product', 'UNIT'])


# marex starts
def get_marex_file():
    for root, dirs, files in os.walk(r'M:\alk_full_rec\ALKAGESTA_DRIVEFILES'): # L:\\
        for file in files:
            if file.endswith('.xlsx'):
                if 'alkagesta_daily_report' in os.path.splitext(os.path.basename(file))[0].lower():
                    if file.split('.')[0][-8:] == date:
                        return file


marex_file = get_marex_file()

try:
    marex_file = os.path.join(r'M:\alk_full_rec\ALKAGESTA_DRIVEFILES', marex_file)
except TypeError:
    marex_file = None

if marex_file is None:
    marex_file_not_found_message = f'Check if Marex file exists on date {date}'
else:
    marex_file_not_found_message = ''


def prepare_marex(file):
    df = read_excel(file, sheetname='OPEN POSITION', rename=None, header=0)
    df = df.iloc[:, 1:]
    df.columns = df.iloc[1]
    df = df.iloc[2:, ]
    df = df.reset_index(drop=True)
    df['Ledger Code'] = df['Ledger Code'].str.strip()
    df = df.loc[~df['Ledger Code'].isin(['08781', '08803', '08989', '09016'])]
    df = df.rename(columns={'Open position date': 'EndDate', 'Instrument Code': 'CODE', 'Volume': 'Quantity',
                            'Delivery/Prompt date': 'Maturity', 'Last Traded date': 'Expiry Date',
                            'Instrument Long Name': 'Instrument', 'Ledger Code': 'ACCOUNT'})
    df['Trade Date'] = pd.to_datetime(df['Trade Date'], errors='coerce')
    df['EndDate'] = pd.to_datetime(df['EndDate'], errors='coerce')
    df['Maturity'] = pd.to_datetime(df['Maturity'], errors='coerce')
    df['Expiry Date'] = pd.to_datetime(df['Expiry Date'], errors='coerce')
    df['Lot Size'] = pd.to_numeric(df['Lot Size'], errors='coerce')
    df['Quantity'] = pd.to_numeric(df['Quantity'], errors='coerce')
    df['Quantity'] = df['Quantity'] * df['Lot Size']
    df['Price'] = pd.to_numeric(df['Price'], errors='coerce')
    df['Price'] = df['Price'].apply(lambda x: round(x, 2))
    df['Variation Margin'] = pd.to_numeric(df['Variation Margin'], errors='coerce')
    columns = ['EndDate', 'Trade Date', 'ACCOUNT', 'Market Code', 'CODE', 'Instrument',
               'Maturity', 'Expiry Date', 'Declaration Date', 'Strike', 'Option Type',
               'Quantity', 'Price', 'Market Rate', 'Variation Margin', 'Currency Code', 'Lot Size']
    df = df[columns]
    df['Maturity'] = df['Maturity'].dt.strftime('%Y-%m')
    return df


try:
    mrx = prepare_marex(marex_file)
except TypeError:
    mrx = pd.DataFrame(columns=['EndDate', 'Trade Date', 'ACCOUNT', 'Market Code', 'CODE',
                                'Instrument', 'Maturity', 'Expiry Date', 'Declaration Date',
                                'Strike', 'Option Type', 'Quantity', 'Price', 'Market Rate',
                                'Variation Margin', 'Currency Code', 'Lot Size'])

mrx_o = mrx.loc[mrx['Option Type'].isin(['C', 'P'])]
mrx_rest = mrx.loc[~mrx['Option Type'].isin(['C', 'P'])]


def marex_manipulation(df):
    df['CLEARER'] = np.nan
    df.loc[:, 'CLEARER'] = 'MAREX'
    df = df.loc[df['Expiry Date'] > df['EndDate']]
    df['CODE'] = df['CODE'].str.strip()
    cols = ['EndDate', 'ACCOUNT', 'CODE', 'Maturity', 'Quantity', 'Price', 'CLEARER']
    df = df[cols]
    return df


try:
    marex_o = marex_manipulation(mrx_o)
except KeyError:
    marex_o = pd.DataFrame(columns=['EndDate', 'ACCOUNT', 'CODE', 'Maturity', 'Quantity', 'Price', 'CLEARER'])

try:
    marex_rest = marex_manipulation(mrx_rest)
except KeyError:
    marex_rest = pd.DataFrame(columns=['EndDate', 'ACCOUNT', 'CODE', 'Maturity', 'Quantity', 'Price', 'CLEARER'])

pm_o = pm_.loc[pm_['TYPE'].isin(['OPTION'])]
pm_rest = pm_.loc[~pm_['TYPE'].isin(['OPTION'])]

marex_o = marex_o.merge(pm_o, on=['CODE'], how='left')
marex_rest = marex_rest.merge(pm_rest, on=['CODE'], how='left')

marex = pd.concat([marex_o, marex_rest])

cols_ = ['EndDate', 'ACCOUNT', 'Maturity', 'Price', 'CLEARER', 'TYPE', 'Reco Product', 'UNIT']
marex = marex[cols_ + ['Quantity']].groupby(cols_).sum().reset_index()


# STONEX NEW

def get_fcstone_file_new():
    for root, dirs, files in os.walk(r'M:\alk_full_rec\ALKAGESTA_DRIVEFILES'): # O:\IFL_ALKAGESTA
        for file in files:
            if file.endswith(".csv"):
                if 'openpositions' in file.lower():
                    if file.split('.')[0][-8:] == date:
                        return file


fcstone_file_new = get_fcstone_file_new()

try:
    fcstone_file_new = os.path.join(r'M:\alk_full_rec\ALKAGESTA_DRIVEFILES', fcstone_file_new) # O:\IFL_ALKAGESTA
except TypeError:
    fcstone_file_new = None

if fcstone_file_new is None:
    new_fcstone_file_not_found_message = f'Check if New FcStone file exists on date {date}'
else:
    new_fcstone_file_not_found_message = ''


def prepare_fcstone_new(file):
    rename = {'Business Date': 'EndDate', 'Maturity Date': 'Maturity', 'Trade Price': 'Price',
              'Instrument Name': 'Instrument', 'Reporting Symbol': 'CODE', 'Contract Size': 'CONTRACT_SIZE',
              'Account Id': 'ACCOUNT', 'Trade Type': 'ProductType', 'Option Style': 'OptionStyle',
              'Strike Price': 'OptionStrike', 'Last Traded Date':'ExpiryDate'}

    df = read_csv(file, rename=rename)
    cols = ['EndDate', 'Instrument', 'CODE', 'Trade Date', 'Maturity', 'CONTRACT_SIZE', 'Quantity', 'Price', 'ACCOUNT',
            'ProductType', 'OptionStyle', 'OptionStrike','ExpiryDate']
    df = df[cols]
    df['CONTRACT_SIZE'] = pd.to_numeric(df['CONTRACT_SIZE'], errors='coerce')
    df['Quantity'] = df['Quantity'].apply(pd.to_numeric, errors='coerce')
    df['Quantity'] = df['Quantity'] * df['CONTRACT_SIZE']
    df['Price'] = df['Price'].apply(pd.to_numeric, errors='coerce')
    df['EndDate'] = pd.to_datetime(df['EndDate'], errors='coerce')
    df['Trade Date'] = pd.to_datetime(df['Trade Date'], errors='coerce')
    df['ExpiryDate'] = pd.to_datetime(df['ExpiryDate'], errors='coerce')
    df['Maturity'] = pd.to_datetime(df['Maturity'], errors='coerce')
    df['CODE'] = df['CODE'].str.strip()
    df['ProductType'] = df['ProductType'].str.strip()
    return df


try:
    fcstone_new = prepare_fcstone_new(fcstone_file_new)
except ValueError:
    fcstone_new = pd.DataFrame(
        columns=['EndDate', 'Instrument', 'CODE', 'Trade Date', 'Maturity', 'CONTRACT_SIZE', 'Quantity', 'Price',
                 'ACCOUNT', 'ProductType', 'OptionStyle', 'OptionStrike', 'ExpiryDate'])

fcstone_new_options = fcstone_new.loc[fcstone_new['ProductType'].str.contains('Call|Put', na=False, regex=True)]
fcstone_new_rest = fcstone_new.loc[~fcstone_new['ProductType'].str.contains('Call|Put', na=False, regex=True)]

### Fc Stone starts ###
fcs_new = fcstone_new.copy()
fcs_report_new = fcs_new.copy()

pm_ = read_product_map(product_file, sheet_name=0)
pm_['TYPE'] = pm_['TYPE'].str.upper()
pm_['UNIT'] = pm_['UNIT'].str.upper()
pm_ = pm_.loc[~pm_['CODE'].isin(['XXX'])]

pm_options = pm_.loc[pm_['TYPE'].isin(['OPTION'])][['CODE', 'TYPE', 'Reco Product', 'UNIT']]
pm_rest = pm_.loc[~pm_['TYPE'].isin(['OPTION'])][['CODE', 'TYPE', 'Reco Product', 'UNIT']]

fcstone_new_options = fcstone_new_options.merge(pm_options, on='CODE', how='left')
fcstone_new_rest = fcstone_new_rest.merge(pm_rest, on='CODE', how='left')
fcs_new = pd.concat([fcstone_new_rest, fcstone_new_options]).reset_index(drop=True)


def fcstone_manipulations_new(df):
    # df = df.loc[df['ExpiryDate'] >= df['EndDate']].reset_index()
    df = df.loc[df['Maturity'].notnull()].reset_index(drop=True)
    df['Maturity'] = df['Maturity'].dt.strftime('%Y-%m')
    df.drop(['Instrument', 'CONTRACT_SIZE', 'CODE', 'Instrument'], axis=1, inplace=True)
    df['CLEARER'] = np.nan
    df.loc[:, 'CLEARER'] = 'NEW INTLFCSTONE'
    df['Price'] = df['Price'].apply(lambda x: round(x, 2))
    return df


try:
    fcs_final_new = fcstone_manipulations_new(fcs_new)
except (KeyError, AttributeError):
    fcs_final_new = pd.DataFrame(
        columns=['EndDate', 'Trade Date', 'Maturity', 'Quantity', 'Price', 'ACCOUNT', 'ProductType', 'OptionStyle',
                 'OptionStrike', 'TYPE', 'Reco Product', 'UNIT', 'CLEARER'])

cols_ = ['EndDate', 'ACCOUNT', 'Maturity', 'Price', 'CLEARER', 'TYPE', 'Reco Product', 'UNIT']
fcs_final_new = fcs_final_new[cols_ + ['Quantity']].groupby(cols_).sum().reset_index()

### merge fcstone, marex & euclid ###
final = pd.concat([fcs_final, euc, marex, fcs_final_new])

pivot = pd.pivot_table(final, index=['TYPE', 'Reco Product', 'UNIT', 'Price', 'Maturity'], values=['Quantity'],
                       columns=['CLEARER'], aggfunc=[np.sum])
pivot = pd.DataFrame(pivot.to_records())

renames = {"('sum', 'Quantity', 'INTLFCSTONE')": "INTLFCSTONE",
           "('sum', 'Quantity', 'EUCLID')": "EUCLID",
           "('sum', 'Quantity', 'MAREX')": "MAREX",
           "('sum', 'Quantity', 'NEW INTLFCSTONE')": "NEW INTLFCSTONE"}

pivot = pivot.rename(columns=renames)

cols = ["TYPE", "Reco Product", 'UNIT', "Maturity", "Price", "INTLFCSTONE", "NEW INTLFCSTONE", "MAREX", "EUCLID"]
pivot = pivot.reindex(cols, axis=1)
pivot['Differences'] = np.nan
pivot['Differences'] = pivot['EUCLID'].sub(
    pivot['MAREX'].add(pivot['INTLFCSTONE'].add(pivot['NEW INTLFCSTONE'], fill_value=0), fill_value=0), fill_value=0)
# result -> pivot
### done ###


### euclid closed positions ###

euclid_cp1 = euclid.loc[euclid['Type'].isin(['OPTION'])]
euclid_cp2 = euclid.loc[~euclid['Type'].isin(['OPTION'])]


def euclid_manipulations_closed_positions(df):
    df.loc[df['Maturity'].isnull(), 'Maturity'] = df['To']
    df['Type'] = df['Type'].str.upper()
    df = df.loc[df['Expiry date'] <= df['EndDate']].reset_index()
    df = df.loc[~(df['Cl. Account'].isin(['LEN33122', '8781', '8803', '8989', '9016']))]  # ~(df['TPA'].isin(['TPA'])
    df.drop(['index'], axis=1, inplace=True)
    df = df.rename(columns={'Cl. Account': 'ACCOUNT', 'Type': 'TYPE', 'Unit': 'UNIT'})
    df['Maturity'] = df['Maturity'].dt.strftime('%Y-%m')
    df.loc[df['TYPE'].isin(['SWAP']), 'Contract'] = df['Commodity B']
    return df


euc_cp2 = euclid_manipulations_closed_positions(euclid_cp2)
euc_cp2['MtM vs Price'] = np.nan
euc_cp2['MtM vs Price'] = euc_cp2['Price'].sub(euc_cp2['MtM'], fill_value=0)
euc_cp2['Commodity A'] = euc_cp2['Commodity A'].map(str)
euc_cp2['Commodity B'] = euc_cp2['Commodity B'].map(str)
euc_cp2 = euc_cp2.merge(pm.drop_duplicates(), left_on=['TYPE', 'Commodity A', 'Commodity B'],
                        right_on=['TYPE', 'Leg1', 'Leg2'], how='left')
euc_cp2.loc[euc_cp2['TYPE'].isin(['CFD']), 'Contract'] = euc_cp2['Reco Product']
euc_cp2.drop(
    ['Reco Product', 'Leg1', 'Leg2', 'From', 'To', 'Commodity A', 'Commodity B', 'Strike', 'Flavor', 'B/S', 'Price A',
     'Price B'], axis=1, inplace=True)
euc_cp2 = euc_cp2.rename(columns={'Contract': 'Reco Product'})
# euc_cp2['MtM vs Price'] = euc_cp2['MtM vs Price'].apply(lambda x: round(x, 2))
euc_cp2['CLEARER'] = 'EUCLID'
euc_cp2['Amount'] = np.nan
euc_cp2['Amount'] = euc_cp2['Quantity'].mul(euc_cp2['MtM vs Price'], fill_value=0)

euclid_cp1['Maturity'] = euclid_cp1['Maturity'].dt.strftime('%Y-%m')
euc_cp1 = euclid_cp1.rename(columns={'Cl. Account': 'ACCOUNT', 'Type': 'TYPE', 'Unit': 'UNIT'})
euc_cp1['Commodity A'] = euc_cp1['Commodity A'].map(str)
euc_cp1['Commodity B'] = euc_cp1['Commodity B'].map(str)
euc_cp1 = euc_cp1.merge(pm, left_on=['TYPE', 'Commodity A', 'Commodity B'], right_on=['TYPE', 'Leg1', 'Leg2'],
                        how='left')
euc_cp1.drop(
    ['Reco Product', 'Leg1', 'Leg2', 'From', 'To', 'Commodity A', 'Commodity B', 'Strike', 'Flavor', 'B/S', 'Price A',
     'Price B'], axis=1, inplace=True)
euc_cp1 = euc_cp1.rename(columns={'Contract': 'Reco Product'})
# euc_cp1['Price'] = euc_cp1['Price'].apply(lambda x: round(x, 2))
euc_cp1['CLEARER'] = 'EUCLID'
euc_cp1['Amount'] = np.nan
euc_cp1['Amount'] = euc_cp1['Quantity'].mul(euc_cp1['Price'], fill_value=0)

euc_cp = pd.concat([euc_cp2, euc_cp1], axis=0)
cols = ['EndDate', 'Expiry date', 'TYPE', 'Reco Product', 'Amount']
euc_cp = euc_cp[cols]
euc_cp_final = euc_cp.groupby(['EndDate', 'Expiry date', 'TYPE', 'Reco Product']).agg(sum).reset_index()
euc_cp_final.loc[euc_cp_final['Amount'] != 0, 'Amount'] = -1 * euc_cp_final['Amount']


### daily balance ###
def prepare_daily_balance(filename):
    df = pd.read_csv(filename)
    cols = ['COB', 'CLEARER', 'SUBACCOUNT', 'BEGIN_CASH_BALANCE', 'END_CASH_BALANCE', 'CREDIT_LINE_NEW_CASH_BALANCE',
            'EXCHANGE_COM', 'EXEC_COM',
            'CLEARING_COM', 'OTHER_COM', 'CREDIT_LINE_PENDING_COM', 'INTERESTS', 'CASH_DEPOSITS', 'CASH_WITHDRAWALS',
            'REALIZED_PNL']
    df = df[cols].drop_duplicates().reset_index(drop=True)
    rename = {'COB': 'EndDate', 'SUBACCOUNT': 'ACCOUNT'}
    df = df.rename(columns=rename)

    df['EndDate'] = pd.to_datetime(df['EndDate'], errors='coerce')
    df['BEGIN_CASH_BALANCE'] = pd.to_numeric(df['BEGIN_CASH_BALANCE'], errors='coerce')
    df['END_CASH_BALANCE'] = pd.to_numeric(df['END_CASH_BALANCE'], errors='coerce')
    df['CREDIT_LINE_NEW_CASH_BALANCE'] = pd.to_numeric(df['CREDIT_LINE_NEW_CASH_BALANCE'], errors='coerce')
    df['EXCHANGE_COM'] = pd.to_numeric(df['EXCHANGE_COM'], errors='coerce')
    df['EXEC_COM'] = pd.to_numeric(df['EXEC_COM'], errors='coerce')
    df['CLEARING_COM'] = pd.to_numeric(df['CLEARING_COM'], errors='coerce')
    df['OTHER_COM'] = pd.to_numeric(df['OTHER_COM'], errors='coerce')
    df['CREDIT_LINE_PENDING_COM'] = pd.to_numeric(df['CREDIT_LINE_PENDING_COM'], errors='coerce')
    df['INTERESTS'] = pd.to_numeric(df['INTERESTS'], errors='coerce')
    df['CASH_DEPOSITS'] = pd.to_numeric(df['CASH_DEPOSITS'], errors='coerce')
    df['CASH_WITHDRAWALS'] = pd.to_numeric(df['CASH_WITHDRAWALS'], errors='coerce')

    df['Difference Cash Balance'] = np.nan
    df['Sum Columns'] = np.nan
    df['Profit/Loss'] = np.nan

    df['Difference Cash Balance'] = df['END_CASH_BALANCE'].sub(df['BEGIN_CASH_BALANCE'], fill_value=0)
    cols = ['CREDIT_LINE_NEW_CASH_BALANCE', 'EXCHANGE_COM', 'EXEC_COM', 'CLEARING_COM', 'OTHER_COM',
            'CREDIT_LINE_PENDING_COM', 'INTERESTS', 'CASH_DEPOSITS', 'CASH_WITHDRAWALS', 'REALIZED_PNL']
    df['Sum Columns'] = df[cols].sum(axis=1)

    df['Profit/Loss'] = df['Difference Cash Balance'].sub(df['Sum Columns'], fill_value=0)

    df['Profit/Loss'] = df['Profit/Loss'].apply(lambda x: round(x, 5))

    re_index = ['EndDate', 'CLEARER', 'ACCOUNT', 'BEGIN_CASH_BALANCE', 'END_CASH_BALANCE', 'Difference Cash Balance',
                'CREDIT_LINE_NEW_CASH_BALANCE',
                'EXCHANGE_COM', 'EXEC_COM', 'CLEARING_COM', 'OTHER_COM', 'CREDIT_LINE_PENDING_COM', 'INTERESTS',
                'CASH_DEPOSITS', 'CASH_WITHDRAWALS', 'REALIZED_PNL', 'Sum Columns', 'Profit/Loss']
    df = df.reindex(re_index, axis=1)
    return df


# def get_daily_balance_file():
#     for root, dirs, files in os.walk(r'K:\\'):
#         for file in files:
#             if file.endswith(".csv"):
#                 if 'dailybalance' in os.path.splitext(os.path.basename(file))[0].lower():
#                     if file.split('_')[0] == date:
#                         return file
#
# daily_balance_file = get_daily_balance_file()
#
# try:
#     db_file = os.path.join('K:\\', daily_balance_file)
# except TypeError:
#     db_file = None


db_file = r'M:\alk_full_rec\20210930_INTLFCSTONE_LEN33120_DailyBalance.csv'

if db_file is None:
    db_file_not_found_message = f'Check if Daily Balance file exists on date {date}'
else:
    db_file_not_found_message = ''

try:
    daily_balance = prepare_daily_balance(db_file)
except ValueError:
    daily_balance = pd.DataFrame(
        columns=['EndDate', 'CLEARER', 'ACCOUNT', 'BEGIN_CASH_BALANCE', 'END_CASH_BALANCE', 'Difference Cash Balance',
                 'CREDIT_LINE_NEW_CASH_BALANCE', 'EXCHANGE_COM', 'EXEC_COM',
                 'CLEARING_COM', 'OTHER_COM', 'CREDIT_LINE_PENDING_COM', 'INTERESTS', 'CASH_DEPOSITS',
                 'CASH_WITHDRAWALS', 'REALIZED_PNL', 'Sum Columns', 'Profit/Loss'])


def get_current_datetime_user():
    now = datetime.now()
    t = now.strftime('%H:%M:%S')
    d = now.strftime('%d %B, %Y')
    user = os.getlogin().upper()
    return f"Latest macros executed at {t} on {d} by {user}"


current_datetime_user = get_current_datetime_user()


def get_missed_code(clearer):
    for cd in list(clearer['CODE'].str.strip().unique()):
        if cd not in list(read_product_map(product_file)['CODE'].unique()):
            return cd


missed_codes_fcstone = get_missed_code(fcs_report)
missed_codes_marex = get_missed_code(mrx)
missed_codes_fcstone_new = get_missed_code(fcs_report_new)

wb = xw.Book(recon_file)


def read_all_db(wb):
    df = pd.DataFrame(wb.sheets['dailybalances'].range('A1').expand('right').expand('down').value)
    df.columns = df.iloc[0]
    df = df.iloc[1:].reset_index(drop=True)
    df['EndDate'] = pd.to_datetime(df['EndDate'], errors='coerce')
    return df


all_daily_balances = read_all_db(wb)
updated_daily_balances = pd.concat([daily_balance, all_daily_balances], axis=0).drop_duplicates().sort_values(
    by='EndDate').reset_index(drop=True)


### marex realized pl ###
def prepare_marex_realized_pl(file):
    df = read_excel(file, sheetname='REALISED P&L', rename=None, header=0)
    df = df.iloc[:, 1:]
    df.columns = df.iloc[1]
    df = df.iloc[2:, ]
    df['Ledger Code'] = df['Ledger Code'].str.strip()
    df = df.loc[~df['Ledger Code'].isin(['08781', '08803', '08989', '09016'])]
    cols = ['Posting Value Date', 'Instrument Long Name', 'Delivery/Prompt date', 'Transaction Volume', 'Posted Amount',
            'Currency Code']
    df = df[cols]
    df['Posting Value Date'] = pd.to_datetime(df['Posting Value Date'], errors='coerce')
    df['Maturity'] = df['Delivery/Prompt date'].str[-6:]
    df['Maturity'] = pd.to_datetime(df['Maturity'], format='%b/%y').dt.strftime('%Y-%m')
    df['Posted Amount'] = pd.to_numeric(df['Posted Amount'], errors='coerce')
    df['Transaction Volume'] = pd.to_numeric(df['Transaction Volume'], errors='coerce')
    df = df.drop(['Delivery/Prompt date'], axis=1)
    df = df.groupby(['Posting Value Date', 'Instrument Long Name', 'Currency Code', 'Maturity']).agg(sum).reset_index()
    return df


try:
    marex_realized_pl = prepare_marex_realized_pl(marex_file)
except:
    marex_realized_pl = pd.DataFrame(
        columns=['Posting Value Date', 'Instrument Long Name', 'Currency Code', 'Maturity', 'Transaction Volume',
                 'Posted Amount'])


def read_realized_pl_marex(wb):
    df = pd.DataFrame(wb.sheets['marex realized pl'].range('A1').expand('right').expand('down').value)
    df.columns = df.iloc[0]
    df = df.iloc[1:].reset_index(drop=True)
    df['Posting Value Date'] = pd.to_datetime(df['Posting Value Date'], errors='coerce')
    df['Transaction Volume'] = pd.to_numeric(df['Transaction Volume'], errors='coerce')
    df['Posted Amount'] = pd.to_numeric(df['Posted Amount'], errors='coerce')
    return df


marex_realized_pl_all = read_realized_pl_marex(wb)
marex_realized_pl = pd.concat([marex_realized_pl, marex_realized_pl_all], axis=0).drop_duplicates().sort_values(
    by='Posting Value Date').reset_index(drop=True)


def get_fcstone_linked_account_posting():
    for root, dirs, files in os.walk(r'M:\alk_full_rec\ALKAGESTA_DRIVEFILES'): # O:\IFL_ALKAGESTA
        for file in files:
            if file.endswith(".csv"):
                if 'linked account postings' in file.lower():
                    if file.split('.')[0][-8:] == date:
                        return file


linked_account_posting = get_fcstone_linked_account_posting()

try:
    linked_account_posting = os.path.join(r'M:\alk_full_rec\ALKAGESTA_DRIVEFILES', linked_account_posting) # O:\IFL_ALKAGESTA
except TypeError:
    linked_account_posting = None

if linked_account_posting is None:
    linked_account_posting_not_found_message = f'Check if New FcStone Linked Account Posting file exists on date {date}'
else:
    linked_account_posting_not_found_message = ''


##### NEW ######
def read_linked_account_posting(file):
    df = pd.read_csv(file)
    df = df.reset_index()
    renames = {'index': 'DATE', 'Posting Action': 'POSTING ACTION', 'Clearing Code': 'PRODUCT', 'Amount': 'REALIZED CASH',
               'Comment': 'COMMENT', 'Delivery Period DDMMYYYY': 'MATURITY', ' Instrument Name': 'QTY'}
    df = df.rename(columns=renames)
    cols = ['DATE', 'POSTING ACTION', 'PRODUCT', 'MATURITY', 'QTY', 'REALIZED CASH', 'COMMENT']
    df = df[cols].reset_index(drop=True)
    df['DATE'] = pd.to_datetime(df['DATE'], errors='coerce')
    df['QTY'] = pd.to_numeric(df['QTY'], errors='coerce')
    df['REALIZED CASH'] = pd.to_numeric(df['REALIZED CASH'], errors='coerce')
    return df
##### NEW ######

# ##### OLD ######
# def read_linked_account_posting(file):
#     df = pd.read_csv(file)
#     renames = {'Unnamed: 0': 'DATE', 'Posting Action': 'POSTING ACTION', 'Clearing Code': 'PRODUCT',
#                'Amount': 'REALIZED CASH',
#                'Comment': 'COMMENT', 'Delivery Period DDMMYYYY': 'MATURITY', ' Instrument Name': 'QTY'}
#     df = df.rename(columns=renames)
#     cols = ['DATE', 'POSTING ACTION', 'PRODUCT', 'MATURITY', 'QTY', 'REALIZED CASH', 'COMMENT']
#     df = df[cols].reset_index(drop=True)
#     df['DATE'] = pd.to_datetime(df['DATE'], errors='coerce')
#     df['QTY'] = pd.to_numeric(df['QTY'], errors='coerce')
#     df['REALIZED CASH'] = pd.to_numeric(df['REALIZED CASH'], errors='coerce')
#     return df
# ##### OLD ######



try:
    stonex_new_realized = read_linked_account_posting(linked_account_posting)
except ValueError:
    stonex_new_realized = pd.DataFrame(
        columns=['DATE', 'POSTING ACTION', 'PRODUCT', 'MATURITY', 'QTY', 'REALIZED CASH', 'COMMENT'])


def prepare_linked_account_posting(df):
    df = df.loc[df['POSTING ACTION'].isin(['P+S', 'Delivery'])]
    cols = ['DATE', 'PRODUCT', 'MATURITY', 'QTY', 'REALIZED CASH']
    df = df[cols]
    df = df.groupby(['DATE', 'PRODUCT', 'MATURITY']).agg(sum).reset_index()
    return df


daily_linked_account_posting = prepare_linked_account_posting(stonex_new_realized)


def read_history_linked_account_posting():
    df = pd.DataFrame(wb.sheets['fcstone linked account posting'].range('A1').expand('right').expand('down').value)
    df.columns = df.iloc[0]
    df = df.iloc[1:].reset_index(drop=True)
    df['DATE'] = pd.to_datetime(df['DATE'], errors='coerce')
    df['QTY'] = pd.to_numeric(df['QTY'], errors='coerce')
    df['REALIZED CASH'] = pd.to_numeric(df['REALIZED CASH'], errors='coerce')
    return df


history_linked_account_posting = read_history_linked_account_posting()
final_linked_account_posting = pd.concat([history_linked_account_posting, daily_linked_account_posting], axis=0). \
    drop_duplicates().sort_values(by='DATE').reset_index(drop=True)


def alka_recon():
    for sh in wb.sheets:

        if sh.name == 'alkarecon':
            sh.range('A14:J14').expand('down').clear_contents()
            sh.range('A13').options(pd.DataFrame, index=False).value = pivot

            sh.range('M8').value = missed_codes_fcstone
            sh.range('M9').value = missed_codes_marex
            sh.range('M10').value = missed_codes_fcstone_new

            sh.range('A1').value = current_datetime_user
            sh.range('A2').value = reports_enddate

            sh.range('A3').clear_contents()
            # sh.range('A3').value = fcstone_file_not_found_message

            sh.range('A4').clear_contents()
            sh.range('A4').value = db_file_not_found_message

            sh.range('A5').clear_contents()
            sh.range('A5').value = marex_file_not_found_message

            sh.range('A6').clear_contents()
            sh.range('A6').value = new_fcstone_file_not_found_message

            sh.range('A7').clear_contents()
            sh.range('A7').value = linked_account_posting_not_found_message

        if sh.name == 'euclid':
            sh.range('A2:O2').expand('down').clear_contents()
            sh.range('A1').options(pd.DataFrame, index=False).value = euclid_report

        if sh.name == 'fcstone':
            sh.range('A2:K2').expand('down').clear_contents()
            sh.range('A1').options(pd.DataFrame, index=False).value = fcs_report

        if sh.name == 'fcstone new':
            sh.range('A2:I2').expand('down').clear_contents()
            sh.range('A1').options(pd.DataFrame, index=False).value = fcs_report_new

        if sh.name == 'marex':
            sh.range('A2:R2').expand('down').clear_contents()
            sh.range('A1').options(pd.DataFrame, index=False).value = mrx

        if sh.name == 'euclid_closedpositions':
            sh.range('A1:E1').expand('down').clear_contents()
            sh.range('A1').options(pd.DataFrame, index=False).value = euc_cp_final

        if sh.name == 'dailybalances':
            sh.range('A1:R1').expand('down').clear_contents()
            sh.range('A1').options(pd.DataFrame, index=False).value = updated_daily_balances

        if sh.name == 'marex realized pl':
            sh.range('A1:F1').expand('down').clear_contents()
            sh.range('A1').options(pd.DataFrame, index=False).value = marex_realized_pl

        if sh.name == 'fcstone linked account posting':
            sh.range('A1:E1').expand('down').clear_contents()
            sh.range('A1').options(pd.DataFrame, index=False).value = final_linked_account_posting

alka_recon()